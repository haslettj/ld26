package me.haslett.ld26;

import org.newdawn.slick.Animation;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.Music;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.SpriteSheet;

public class Game extends BasicGame {

	public static final String POTATO_SPRITESHEET_FILENAME = "res/potato.png";
	public static final String TALISMAN_SPRITESHEET_FILENAME = "res/talismans.png";
	public static final String IMAGE_GAMEOVER_FILENAME = "res/gameover.png";
	public static final String BLOCK_SPRITESHEET_FILENAME = "res/blocks.png";
	public static final String SOUND_JUMP_FILENAME = "res/jump.wav";
	public static final String SOUND_REDBOUNCE_FILENAME = "res/redbounce.wav";
	public static final String SOUND_REDTHROW_FILENAME = "res/redthrow.wav";
	public static final String SOUND_COLLECT_TALISMAN_FILENAME = "res/collecttalisman.wav";
	public static final String MUSIC_FILENAME = "res/music.wav";
	public static final String TESTMAP_FILENAME = "res/testmap.txt";
	public static final String[] LEVEL_FILENAMES = new String[] { "res/level-1.txt", "res/level-2.txt", "res/level-3.txt", "res/level-4.txt" };
	public static final String TITLE = "Chromatater";
	public static final float INFO_AREA_HEIGHT = 40;
	public static final float WIDTH = 640;
	public static final float HEIGHT = 480;
	public static final boolean TALISMAN_COLLECTED_STARTING_STATE = false;

	public static final int GAMEOVER_TEXT_LENGTH = 2;
	public static final String[] GAMEOVER_TEXT = new String[] { "You seem to have fallen through the world", "Press the Space Bar to try again" };
	public static final String[] PAUSED_TEXT = new String[] { "Welcome to " + TITLE, "", "", "Move the character right or left using arrow keys or 'a' and 'd'.", "Jump using up arrow or 'w'",
			"Activate Talismans with '1' for Red, '2' for Green, '3' for Blue", "Press SPACE BAR within the game to pause", "", "", "The object of the game is to escape the maze.",
			"Colored blocks behave differently depending on the state of the", "corresponding colored Talisman.", "You must collect each Talisman before it can be used.",
			"Good Luck, and I hope you enjoy this game!", "", "", "Press SPACE BAR to continue from this screen." };

	public static final String[] WIN_TEXT = new String[] { "You got all the Potatoes!  You Win!", "", "", "This game was made in under 48 hours for Ludum Dare #26.", "", "",
			"If you enjoyed playing it, please rate appropriately.", "", "", "Thank you for Playing!", "", "", "", "", "Press SPACE BAR if you would like to play again." };

	protected static final String MAP_IMAGE = "res/background.png";
	protected static final int SCREEN_BORDER = 80;

	protected SpriteSheet blockSprites;
	protected Animation[] blockAnimations;

	protected SpriteSheet talismanSprites;
	protected Animation[] talismanAnimations;

	protected SpriteSheet potatoSprites;
	protected Animation potatoAnimation;

	protected Image gameOverImage;

	protected Level level;
	protected Camera camera;
	protected Player player;
	protected Talisman[] talismans;

	protected int currentLevel;

	protected Music music;

	protected boolean gameStateGameOver;
	protected boolean gameStatePaused;
	protected boolean gameStateWin;

	public Game() {
		super( TITLE );
		currentLevel = 0;
	}

	protected void levelUp() throws Exception {
		gameStatePaused = true;
		currentLevel++;
		if ( currentLevel < LEVEL_FILENAMES.length ) {
			level = Level.loadFromFile( LEVEL_FILENAMES[currentLevel], blockSprites, blockAnimations, talismanSprites, talismanAnimations, potatoSprites, potatoAnimation );
			player.setLocation( level.getSpawnBlockX() * Block.SIZE, level.getSpawnBlockY() * Block.SIZE );
			
			player.setLevel(level);
			
			
			for ( int i = 0; i < 4; i++ ) {
				talismans[i].setIsCollected( TALISMAN_COLLECTED_STARTING_STATE );
				talismans[i].setActivationStatus( Talisman.BEHAVIOR_DEFAULT );
			}
			
			for(int i = 0; i < Block.ANIMATION_COUNT; i++){
				blockAnimations[i].setCurrentFrame( Block.ANIMATION_INDEX_DEFAULT );
			}
			camera.setLocation( 0, 0 );
			
			level.setTalismans( talismans );

		} else {
			gameStateWin = true;
		}
		gameStatePaused = false;
		player.setHasPotato( false );
	}

	@Override
	public void render( GameContainer gc, Graphics g ) throws SlickException {

		if ( gameStateGameOver ) {
			gameOverImage.draw( 0, 0 );
			g.setColor( Color.white );
			for ( int i = 0; i < GAMEOVER_TEXT_LENGTH; i++ ) {
				g.drawString( GAMEOVER_TEXT[i], 20, 350 + i * 30 );
			}

		} else if ( gameStateWin ) {
			level.getBackground().draw( 0, 0 );
			g.setColor( Color.white );
			int line = 0;
			for ( String s : WIN_TEXT ) {
				g.drawString( s, 20, 50 + 20 * line++ );
			}
		} else if ( gameStatePaused ) {
			level.getBackground().draw( 0, 0 );
			g.setColor( Color.white );
			int line = 0;
			for ( String s : PAUSED_TEXT ) {
				g.drawString( s, 20, 50 + 20 * line++ );
			}

		} else {
			level.render( gc, g, camera, INFO_AREA_HEIGHT );

			g.setColor( Color.black );
			g.fillRect( 0, 0, WIDTH - 115, INFO_AREA_HEIGHT );

			g.setColor( Color.white );
			g.drawString( level.getDisplayText(), 5, 10 );
			
			g.setColor( Color.pink );

			// g.drawString( String.valueOf( player.getVelocityY() ), 10, 50 );
			// g.drawString( camera.getDebug(), 10, 50 );
			// g.drawString( player.getDebug(), 10, 60 );
			// g.drawString( "Last Box On: " + String.valueOf(
			// player.getLastBoxStoodOn() ), 10, 70 );

			player.render( gc, g, camera, INFO_AREA_HEIGHT, -1 );

			for ( int i = 0; i < 4; i++ ) {
				if ( talismans[i].getIsCollected() ) {
					talismans[i].renderStatus( (int) WIDTH - ( 5 + Block.SIZE ) * ( 4 - i ), 5, gc, g, blockAnimations[i] );
				}
			}
			
			g.setColor( Color.white );
			g.drawString( level.getDisplayText(), 5, 10 );
		}

	}

	@Override
	public void init( GameContainer gc ) throws SlickException {

		gc.setMinimumLogicUpdateInterval( 16 );
		gc.setMaximumLogicUpdateInterval( 16 );

		gc.setShowFPS( false );

		try {
			gameOverImage = new Image( IMAGE_GAMEOVER_FILENAME );

			blockSprites = new SpriteSheet( BLOCK_SPRITESHEET_FILENAME, Block.SIZE, Block.SIZE );

			blockAnimations = new Animation[Block.ANIMATION_COUNT];
			for ( int i = 0; i < Block.ANIMATION_COUNT; i++ ) {
				blockAnimations[i] = new Animation( blockSprites, 0, i, Block.ANIMATION_FRAME_COUNTS[i] - 1, i, true, Block.ANIMATION_DURATIONS[i], true );
				blockAnimations[i].setPingPong( Block.ANIMATION_PING_PONG[i] );
				blockAnimations[i].stop();
				blockAnimations[i].setCurrentFrame( 0 );
			}

			talismanSprites = new SpriteSheet( TALISMAN_SPRITESHEET_FILENAME, Block.SIZE, Block.SIZE );
			talismanAnimations = new Animation[Talisman.ANIMATION_COUNT];
			for ( int i = 0; i < Talisman.ANIMATION_COUNT; i++ ) {
				talismanAnimations[i] = new Animation( talismanSprites, 0, i, Talisman.ANIMATION_FRAME_COUNTS[i] - 1, i, true, Talisman.ANIMATION_DURATIONS[i], true );
				talismanAnimations[i].setPingPong( Talisman.ANIMATION_PING_PONG[i] );
				talismanAnimations[i].setLooping( true );
			}

			potatoSprites = new SpriteSheet( POTATO_SPRITESHEET_FILENAME, Block.SIZE, Block.SIZE );
			potatoAnimation = new Animation( potatoSprites, 0, 0, 2, 0, true, 200, true );
			potatoAnimation.setPingPong( true );
			potatoAnimation.setLooping( true );

			level = Level.loadFromFile( LEVEL_FILENAMES[currentLevel], blockSprites, blockAnimations, talismanSprites, talismanAnimations, potatoSprites, potatoAnimation );

		} catch ( Exception e ) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		camera = new Camera( 0, 0, WIDTH, HEIGHT );

		player = new Player( level.getSpawnBlockX() * Block.SIZE - Player.HIT_BOX_X + 0.1f, level.getSpawnBlockY() * Block.SIZE - 0.1f, Player.WIDTH, Player.HEIGHT, level );
		player.setJumpSound( new Sound( SOUND_JUMP_FILENAME ) );
		player.setRedbounceSound( new Sound( SOUND_REDBOUNCE_FILENAME ) );
		player.setRedthrowSound( new Sound( SOUND_REDTHROW_FILENAME ) );
		player.setCollectTalismanSound( new Sound( SOUND_COLLECT_TALISMAN_FILENAME ) );

		talismans = new Talisman[4];

		for ( int i = 0; i < 4; i++ ) {
			talismans[i] = new Talisman( 0, 0, i, talismanSprites, talismanAnimations );
			talismans[i].setIsCollected( TALISMAN_COLLECTED_STARTING_STATE );
		}

		talismans[0].setIsCollected( false );

		level.setTalismans( talismans );

		// try {
		// Talisman t = new Talisman( 1, 3, Entity.COLOR_BIT_RED,
		// talismanAnimations );
		// int key = level.addEntity( t );
		// t.setIndex( key );
		// t.setLevel( level );
		//
		// } catch ( Exception e ) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		gameStateGameOver = false;
		gameStatePaused = true;
		gameStateWin = false;

		music = new Music( MUSIC_FILENAME );

	}

	@Override
	public void update( GameContainer gc, int delta ) throws SlickException {
		Input input = gc.getInput();

		if ( gameStateGameOver || gameStatePaused ) {
			music.stop();
			if ( input.isKeyPressed( Input.KEY_SPACE ) ) {

				if ( gameStateGameOver || gameStateWin ) {
					currentLevel = 0;
					init( gc );
				}

				gameStateGameOver = false;
				gameStatePaused = false;
				gameStateWin = false;

				music.loop();

			}
		} else {
			if ( input.isKeyPressed( Input.KEY_1 ) && talismans[Talisman.INDEX_RED].getIsCollected() ) {
				if ( input.isKeyDown( Input.KEY_LSHIFT ) ) {
					talismans[Talisman.INDEX_RED].decrementActivationStatus();
				} else {
					talismans[Talisman.INDEX_RED].incrementActivationStatus();
				}
				blockAnimations[Entity.COLOR_BIT_RED + Block.ANIMATION_OFFSET_TO_COLOR].setCurrentFrame( talismans[Talisman.INDEX_RED].getActiviationStatus() );
			}
			if ( input.isKeyPressed( Input.KEY_2 ) && talismans[Talisman.INDEX_GREEN].getIsCollected() ) {
				if ( input.isKeyDown( Input.KEY_LSHIFT ) ) {
					talismans[Talisman.INDEX_GREEN].decrementActivationStatus();
				} else {
					talismans[Talisman.INDEX_GREEN].incrementActivationStatus();
				}
				blockAnimations[Entity.COLOR_BIT_GREEN + Block.ANIMATION_OFFSET_TO_COLOR].setCurrentFrame( talismans[Talisman.INDEX_GREEN].getActiviationStatus() );
			}
			if ( input.isKeyPressed( Input.KEY_3 ) && talismans[Talisman.INDEX_BLUE].getIsCollected() ) {
				if ( input.isKeyDown( Input.KEY_LSHIFT ) ) {
					talismans[Talisman.INDEX_BLUE].decrementActivationStatus();
				} else {
					talismans[Talisman.INDEX_BLUE].incrementActivationStatus();
				}
				blockAnimations[Entity.COLOR_BIT_BLUE + Block.ANIMATION_OFFSET_TO_COLOR].setCurrentFrame( talismans[Talisman.INDEX_BLUE].getActiviationStatus() );
			}

			if ( input.isKeyPressed( Input.KEY_W ) || input.isKeyPressed( Input.KEY_UP ) )
				player.move( Player.MOVE_DIRECTION_UP, level );
			if ( input.isKeyDown( Input.KEY_D ) || input.isKeyDown( Input.KEY_RIGHT ) )
				player.move( Player.MOVE_DIRECTION_RIGHT, level );
			if ( input.isKeyDown( Input.KEY_A ) || input.isKeyDown( Input.KEY_LEFT ) )
				player.move( Player.MOVE_DIRECTION_LEFT, level );
			if ( input.isKeyPressed( Input.KEY_SPACE ) )
				gameStatePaused = true;

			player.update( level );

			if ( player.getVelocityY() > 75 ) {
				gameStateGameOver = true;

			}

			float player_col_min_x = player.getWorldCollisionBox().getMinX();
			float player_col_max_x = player.getWorldCollisionBox().getMaxX();

			if ( player_col_max_x + SCREEN_BORDER > camera.getMaxX() ) {
				camera.setX( player_col_max_x + SCREEN_BORDER - camera.getWidth() );
				if ( camera.getMaxX() > level.getBackground().getWidth() )
					camera.setX( level.getBackground().getWidth() - camera.getWidth() );
			} else if ( player_col_min_x < camera.getMinX() + SCREEN_BORDER ) {
				camera.setX( player_col_min_x - SCREEN_BORDER );

				if ( camera.getX() < 0 )
					camera.setX( 0 );
			}

			float player_col_min_y = player.getWorldCollisionBox().getMinY();
			float player_col_max_y = player.getWorldCollisionBox().getMaxY();

			if ( player_col_max_y + SCREEN_BORDER > camera.getMaxY() ) {
				camera.setY( player_col_max_y + SCREEN_BORDER - camera.getHeight() );

			} else if ( player_col_min_y < camera.getMinY() + SCREEN_BORDER ) {
				camera.setY( player_col_min_y - SCREEN_BORDER );
				if ( camera.getY() < 0 )
					camera.setY( 0 );
			}

			if ( player.hasPotato ) {
				try {
					levelUp();
				} catch ( Exception e ) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public static void main( String[] args ) throws SlickException {
		AppGameContainer app = new AppGameContainer( new Game(), (int) Game.WIDTH, (int) Game.HEIGHT, false );

		app.start();
	}
}
