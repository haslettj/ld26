package me.haslett.ld26;

import java.util.ArrayList;

import org.newdawn.slick.Sound;

import me.haslett.collections.MapEntry;

public abstract class MovableEntity extends Entity {

	public static final int RED_X_VELOCITY_MULTIPLIER = -2;
	public static final int RED_X_VELOCITY_CONSTANT = 16;
	public static final int RED_Y_VELOCITY_CONSTANT = 16;
	public static final int TICKS_TO_PLAY_RIGHT_LEFT_ANIMATION = 10;
	public static final int MOVE_DIRECTION_UP = 1;
	public static final int MOVE_DIRECTION_DOWN = 2;
	public static final int MOVE_DIRECTION_LEFT = 4;
	public static final int MOVE_DIRECTION_RIGHT = 8;

	protected float velocityX = 0;
	protected float velocityY = 0;

	protected float moveAccel = 0.0f;
	protected float moveMaxSpeed = 0.0f;
	protected float jumpForce = 0.0f;

	protected boolean isJumping;
	protected boolean isFalling;

	protected int lastBoxStoodOn = -1;

	protected int rightleftAnimationCounter = 0;

	protected Sound jumpSound;
	protected Sound redbounceSound;
	protected Sound redthrowSound;
	protected Sound collectTalismanSound;

	public MovableEntity( float x, float y, float width, float height ) {
		super( x, y, width, height );

	}

	public int getLastBoxStoodOn() {
		return lastBoxStoodOn;
	}

	public float getVelocityY() {
		return velocityY;
	}

	public float getVelocityX() {
		return velocityX;
	}

	public void setJumpSound( Sound sound ) {
		jumpSound = sound;
	}

	public void setRedbounceSound( Sound sound ) {
		redbounceSound = sound;
	}

	public void setRedthrowSound( Sound sound ) {
		redthrowSound = sound;
	}

	public void setCollectTalismanSound( Sound sound ) {
		collectTalismanSound = sound;
	}

	@Override
	public void update( Level level ) {

		super.update( level );

		velocityY += level.getGravity();

		isFalling = false;

		if ( velocityY >= 0 ) {
			isFalling = true;
			isJumping = false;
		}

		y += velocityY;

		// Process Falling and Jumping:
		boolean hitRed = false;
		Talisman redTalisman = level.getTalisman( Talisman.INDEX_RED );

		if ( isFalling ) {
			ArrayList<MapEntry<Integer, Entity>> collisions = level.CollidesWith( this );

			float yOffset = hitBox.getMaxY();

			for ( MapEntry<Integer, Entity> map : collisions ) {
				float mapY = map.getValue().getWorldCollisionBox().getY();

				if ( y + yOffset > mapY ) {
					velocityY = 0;
					y = mapY - yOffset - 0.1f;
					isJumping = false;
					isFalling = false;

					lastBoxStoodOn = map.getKey();
				}
				if ( map.getValue().getColor() == Entity.COLOR_BIT_RED && !hitRed && redTalisman.getActiviationStatus() == Talisman.BEHAVIOR_DEFAULT ) {
					velocityX *= RED_X_VELOCITY_MULTIPLIER;
					velocityY = -RED_Y_VELOCITY_CONSTANT;
					isJumping = true;
					hitRed = true;
					if ( redthrowSound != null )
						redthrowSound.play();
				}

			}

		} else if ( isJumping ) {
			ArrayList<MapEntry<Integer, Entity>> collisions = level.CollidesWith( this );

			for ( MapEntry<Integer, Entity> map : collisions ) {
				float mapMaxY = map.getValue().getWorldCollisionBox().getMaxY();

				if ( y < mapMaxY ) {
					velocityY = 0;
					y = mapMaxY + 1;
					isJumping = false;
					isFalling = true;
				}
				if ( map.getValue().getColor() == Entity.COLOR_BIT_RED && !hitRed && redTalisman.getActiviationStatus() == Talisman.BEHAVIOR_DEFAULT ) {
					velocityX *= RED_X_VELOCITY_MULTIPLIER;
					velocityY = 0;
					isFalling = true;
					isJumping = false;
					hitRed = true;
					if ( redthrowSound != null )
						redthrowSound.play();
				}
			}
		}

		// Process move right or left

		if ( velocityX != 0 ) {
			float xOffset = hitBox.getMaxX();
			float yOffset = hitBox.getMaxY();

			int direction;

			x += velocityX;

			if ( velocityX > 0 ) {
				direction = MOVE_DIRECTION_RIGHT;
				if ( !( isJumping || isFalling ) ) {
					velocityX -= level.getFriction( level.getEntity( lastBoxStoodOn ).getColor() );
				}
				if ( velocityX < 0 ) {
					velocityX = 0;
					rightleftAnimationCounter = 0;
				}
			} else {
				direction = MOVE_DIRECTION_LEFT;
				if ( !( isJumping || isFalling ) ) {
					velocityX += level.getFriction( level.getEntity( lastBoxStoodOn ).getColor() );
				}
				if ( velocityX > 0 ) {
					velocityX = 0;
					rightleftAnimationCounter = 0;
				}
			}

			hitRed = false;

			ArrayList<MapEntry<Integer, Entity>> collisions = level.CollidesWith( this );
			for ( MapEntry<Integer, Entity> map : collisions ) {
				float collisionMinX = map.getValue().getWorldCollisionBox().getX();
				float collisionMinY = map.getValue().getWorldCollisionBox().getY();
				float collisionMaxY = map.getValue().getWorldCollisionBox().getMaxY();

				if ( ( collisionMinY > y && collisionMinY < y + yOffset ) || ( collisionMaxY > y && collisionMaxY < y + yOffset ) || ( y > collisionMinY && y < collisionMaxY )
						|| ( y + yOffset > collisionMinY && y + yOffset < collisionMaxY ) ) {

					if ( direction == MOVE_DIRECTION_RIGHT ) {
						x = collisionMinX - xOffset - 1;
						velocityX = 0;
						if ( map.getValue().getColor() == Entity.COLOR_BIT_RED && redTalisman.getActiviationStatus() == Talisman.BEHAVIOR_DEFAULT ) {
							velocityX = -RED_X_VELOCITY_CONSTANT;
							velocityY = -7;
							isJumping = true;
							hitRed = true;
							if ( redbounceSound != null )
								redbounceSound.play();
						}
					} else if ( direction == MOVE_DIRECTION_LEFT ) {
						x = map.getValue().getWorldCollisionBox().getMaxX() + 1 - hitBox.getX();
						velocityX = 0;
						if ( map.getValue().getColor() == Entity.COLOR_BIT_RED && redTalisman.getActiviationStatus() == Talisman.BEHAVIOR_DEFAULT ) {
							velocityX = RED_X_VELOCITY_CONSTANT;
							velocityY = -7;
							isJumping = true;
							hitRed = true;
							if ( redbounceSound != null )
								redbounceSound.play();
						}
					}

				}
			}

		}
	}

	public void move( int direction, Level level ) {

		float maxLocalMoveAcceleration = moveAccel < level.getFriction( level.getEntity( lastBoxStoodOn ).getColor() ) ? moveAccel : level.getFriction( level.getEntity( lastBoxStoodOn ).getColor() );

		maxLocalMoveAcceleration *= 2.0f; // acceleration can be double
											// friction, no more!

		switch ( direction ) {
		case MOVE_DIRECTION_RIGHT:
			if ( !( isJumping || isFalling ) || ( Math.abs( velocityX ) < 0.01f ) ) {

				velocityX += isJumping && maxLocalMoveAcceleration < 0.1f ? 0.1f : maxLocalMoveAcceleration;
			}
			if ( velocityX > moveMaxSpeed )
				velocityX = moveMaxSpeed;
			rightleftAnimationCounter = TICKS_TO_PLAY_RIGHT_LEFT_ANIMATION;
			break;

		case MOVE_DIRECTION_LEFT:
			if ( !( isJumping || isFalling ) || ( Math.abs( velocityX ) < 0.01f ) ) {
				velocityX -= isJumping && maxLocalMoveAcceleration < 0.1f ? 0.1f : maxLocalMoveAcceleration;
			}
			if ( velocityX < -moveMaxSpeed )
				velocityX = -moveMaxSpeed;
			rightleftAnimationCounter = TICKS_TO_PLAY_RIGHT_LEFT_ANIMATION;
			break;

		case MOVE_DIRECTION_UP:
			if ( !( isJumping || isFalling )
					&& !( level.getEntity( lastBoxStoodOn ).getColor() == Entity.COLOR_BIT_GREEN && level.getTalisman( Talisman.INDEX_GREEN ).getActiviationStatus() == Talisman.BEHAVIOR_DEFAULT ) ) {
				velocityY -= jumpForce;
				isJumping = true;
				if ( jumpSound != null ) {
					jumpSound.play();
				}
			}
			break;

		default:
			break;
		}
	}
}
