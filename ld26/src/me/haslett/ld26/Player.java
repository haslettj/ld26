package me.haslett.ld26;

import org.newdawn.slick.Animation;
import org.newdawn.slick.Color;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

public class Player extends MovableEntity {

	public static final String NAME = "Player";
	public static final int HIT_BOX_WIDTH = 24;
	public static final int HIT_BOX_X = 7;
	public static final String PLAYER_SPRITES_FILE_NAME = "res/player.png";
	public static final float JUMP_FORCE = 7.0f;
	public static final float MOVE_ACCELERATION = 1.0f;
	public static final float MOVE_MAX_SPEED = 4.0f;
	public static final int ANIMATION_COUNT = 5;
	public static final int ANIMATION_IDLE = 0;
	public static final int ANIMATION_RUN_RIGHT = 1;
	public static final int ANIMATION_RUN_LEFT = 2;
	public static final int ANIMATION_JUMP = 3;
	public static final int ANIMATION_FALL = 4;
	public static final int[] ANIMATION_FRAME_COUNTS = new int[] { 3, 4, 4, 2, 2 };
	public static final int[] ANIMATION_DURATIONS = new int[] { 200, 200, 200, 100, 200 };
	public static final boolean[] ANIMATION_PING_PONG = new boolean[] { true, false, false, false, true };
	public static final boolean[] ANIMATION_LOOPING = new boolean[] { true, true, true, false, true };

	public static final float WIDTH = 40f;
	public static final float HEIGHT = 53f;

	protected Level level;
	protected boolean hasPotato;

	public Player( float x, float y, float width, float height, Level level ) throws SlickException {
		super( x, y, width, height );

		_name = NAME;

		hitBoxColor = Color.pink;

		animationSheet = new SpriteSheet( PLAYER_SPRITES_FILE_NAME, (int) WIDTH, (int) HEIGHT );
		animations = new Animation[ANIMATION_COUNT];
		for ( int i = 0; i < ANIMATION_COUNT; i++ ) {
			animations[i] = new Animation( animationSheet, 0, i, ANIMATION_FRAME_COUNTS[i] - 1, i, true, ANIMATION_DURATIONS[i], true );
			animations[i].setPingPong( ANIMATION_PING_PONG[i] );
			animations[i].setLooping( ANIMATION_LOOPING[i] );
		}

		currentAnimation = ANIMATION_IDLE;

		moveAccel = MOVE_ACCELERATION;
		moveMaxSpeed = MOVE_MAX_SPEED;
		jumpForce = JUMP_FORCE;

		hitBox.setX( HIT_BOX_X );
		hitBox.setWidth( HIT_BOX_WIDTH );

		color = COLOR_BIT_BLACK;

		this.level = level;
		this.hasPotato = false;
	}

	public boolean getHasPotato() {
		return hasPotato;
	}

	public void setHasPotato( boolean value ) {
		hasPotato = value;
	}

	public void setLocation( float x, float y ) {
		this.x = x;
		this.y = y;
		this.velocityX = 0;
		this.velocityY = 0;
		isJumping = false;
		isFalling = false;
		
	}

	public void setLevel( Level level ) {
		this.level = level;
	}

	@Override
	public void update( Level level ) {
		super.update( level );

		if ( isJumping ) {
			currentAnimation = ANIMATION_JUMP;
		} else if ( isFalling ) {
			currentAnimation = ANIMATION_FALL;
		} else if ( velocityX > 0 && rightleftAnimationCounter > 0 ) {
			currentAnimation = ANIMATION_RUN_RIGHT;
			rightleftAnimationCounter--;
		} else if ( velocityX < 0 && rightleftAnimationCounter > 0 ) {
			currentAnimation = ANIMATION_RUN_LEFT;
			rightleftAnimationCounter--;
		} else {
			currentAnimation = ANIMATION_IDLE;
		}
	}

	@Override
	public void collectTalisman( Talisman talisman ) {
		if ( talisman.getName() == "Potato" ) {
			this.hasPotato = true;
		} else {
			level.talismans[talisman.color - 1].setIsCollected( true );
			if ( collectTalismanSound != null )
				collectTalismanSound.play();
		}
	}

	public void respawn( float x, float y ) {
		velocityX = 0f;
		velocityY = 0f;
		this.x = x;
		this.y = y;
	}

}
